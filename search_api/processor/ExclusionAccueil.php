<?php

namespace Drupal\cspw_recherche\Plugin\search_api\processor;

use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\node\Entity\Node;

/**
 * Exclut la page d'accueil.
 *
 * @SearchApiProcessor(
 *   id = "exclusion_accueil",
 *   label = @Translation("Home exclusion"),
 *   description = @Translation("Exclude home page."),
 *   stages = {
 *     "alter_items" = 0,
 *   },
 * )
 */
class ExclusionAccueil extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function alterIndexedItems(array &$items) {
    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item_id => $item) {
      $objet = $item->getOriginalObject()->getValue();
      if ($objet instanceof Node) {
        if ($objet->getType() == 'page_standard') {
          if (isset($objet->field_home_page->value)) {
            $index_option = $objet->field_home_page->value;
          }
          else {
            $index_option = FALSE;
          }
          if ($index_option) {
            unset($items[$item_id]);
          }
        }
      }
    }
  }

}
