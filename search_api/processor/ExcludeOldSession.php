<?php

namespace Drupal\cspw_recherche\Plugin\search_api\processor;

use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\node\Entity\Node;
use Drupal\Core\Datetime\DrupalDateTime;


/**
 * Exclut la page d'accueil.
 *
 * @SearchApiProcessor(
 *   id = "exclusion_old_session",
 *   label = @Translation("old session exclusion"),
 *   description = @Translation("Exclude old session page."),
 *   stages = {
 *     "alter_items" = -50,
 *   },
 * )
 */
class ExcludeOldSession extends ProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function alterIndexedItems(array &$items) {
    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item_id => $item) {
      $objet = $item->getOriginalObject()->getValue();
      if ($objet instanceof Node) {
        if ($objet->getType() == 'salons_sessions') {
          if (isset($objet->field_date_de_fin->value)) {
            $session_date = $objet->field_date_de_fin->value;
            $dateTime =  new DrupalDateTime($session_date);
            $dateTime_session = $dateTime->format('U');
            $today = date("Y-m-d");
            $today_date_time =  new DrupalDateTime($today);
            $today_date_time = $today_date_time->format('U');
            if($today_date_time > $dateTime_session){
              unset($items[$item_id]);
            }
          }

        }
      }
    }
  }

}
